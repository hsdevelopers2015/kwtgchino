$(window).load(function(){
    var wwidth = $(window).width();
    if(wwidth > 1199){
        var waypoints = new Waypoint({
          element: $('#waypoint-one'),
          handler: function(direction) {
            if(direction == 'up'){
                $('#waypoint-one').css({"position": "relative", "top": "auto", "margin-right": "20px"});
            } else {
                $('#waypoint-one').css({"position": "fixed", "top": "50px"});
            }
        },
        });
        var waypointstwo = new Waypoint({
            element: $('#waypoint-two'),
            handler: function(direction) {
            if(direction == 'up'){
                $('#img-2').fadeOut('slow');
                $('#img-1').fadeIn('slow');
            } else {
                $('#img-1').fadeOut('slow');
                $('#img-2').fadeIn('slow');
            }
        },
            offset: 350
        });
        var waypointsthree = new Waypoint({
            element: $('#waypoint-three'),
            handler: function(direction) {
            if(direction == 'up'){
                $('#img-3').fadeOut('slow');
                $('#img-2').fadeIn('slow');
            } else {
                $('#img-2').fadeOut('slow');
                $('#img-3').fadeIn('slow');
            }
        },
            offset: 350
        });
        var waypointsfour = new Waypoint({
            element: $('#waypoint-four'),
            handler: function(direction) {
            if(direction == 'up'){
                $('#btm-img').fadeOut('fast');
                $('#waypoint-one').fadeIn('fast');
            } else {
                $('#waypoint-one').css('display', 'none');
                $('#btm-img').fadeIn('fast');
            }
        },
            offset: 281
        });
    }

});

$('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') 
        || location.hostname == this.hostname) {

        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
           if (target.length) {
             $('html,body').animate({
                 scrollTop: target.offset().top
            }, 1000);
            return false;
        }
    }
});

$(document).ready(function(){
    $('.club-list-btn > p').on('click', function(){
        var dd_elem = $(".dropdown-element");
        var elem = $(this);
        var parent = elem.parents("div");

        console.log(parent.html())
        // if(dd_elem.css("display") == "block")
        dd_elem.slideToggle(500, function(){
            $(this).stop(true, true);

            if(dd_elem.css("display") == "block"){
                var arrow = $("<i class='fa fa-chevron-up'></i>");
                elem.html(arrow);
            }else{

                elem.text(parent.data('orig_text'))
            }

        });
    });

});

$(document).ready(function(){
    $('.seehow > p').on('click', function(){
        var dd_elem = $(".dropdown-element-video");
        var elem = $(this);
        var parent = elem.parents("div");
        console.log(parent.html())
        // if(dd_elem.css("display") == "block")
        dd_elem.slideToggle(500, function(){
            $(this).stop(true, true);

            if(dd_elem.css("display") == "block"){
                var arrow = $("<i class='fa fa-chevron-up'></i>");
                elem.html(arrow);
                Waypoint.refreshAll();

            }else{

                elem.text(parent.data('orig_text'))
                Waypoint.refreshAll();

            }

        });
    });
});

$(document).ready(function(){
    $(window).delegate("document","scroll",function(){
    // $(window).scroll(function () {
        console.log($(window).scrollTop());
        var topDivHeight = $("#main-header").height();
        var viewPortSize = $(window).height();

        var triggerAt = 150;
        var triggerHeight = (topDivHeight - viewPortSize) + triggerAt;

        if ($(window).scrollTop() >= triggerHeight) {
            $('.sec-img').css('visibility', 'visible').hide().fadeIn();
            $(this).off('scroll');
        }
        var wwidth = $(window).width();
        if(wwidth > 1199){

            var waypoints = new Waypoint({
              element: $('#waypoint-one'),
              handler: function(direction) {
                if(direction == 'up'){
                    $('#waypoint-one').css({"position": "relative", "top": "auto", "margin-right": "20px"});
                } else {
                    $('#waypoint-one').css({"position": "fixed", "top": "50px"});
                }
            },
            });
            var waypointstwo = new Waypoint({
                element: $('#waypoint-two'),
                handler: function(direction) {
                if(direction == 'up'){
                    $('#img-2').fadeOut('slow');
                    $('#img-1').fadeIn('slow');
                } else {
                    $('#img-1').fadeOut('slow');
                    $('#img-2').fadeIn('slow');
                }
            },
                offset: 350
            });
            var waypointsthree = new Waypoint({
                element: $('#waypoint-three'),
                handler: function(direction) {
                if(direction == 'up'){
                    $('#img-3').fadeOut('slow');
                    $('#img-2').fadeIn('slow');
                } else {
                    $('#img-2').fadeOut('slow');
                    $('#img-3').fadeIn('slow');
                }
            },
                offset: 350
            });
            var waypointsfour = new Waypoint({
                element: $('#waypoint-four'),
                handler: function(direction) {
                if(direction == 'up'){
                    $('#btm-img').fadeOut('fast');
                    $('#waypoint-one').fadeIn('fast');
                } else {
                    $('#waypoint-one').css('display', 'none');
                    $('#btm-img').fadeIn('fast');
                }
            },
                offset: 281
            });
            Waypoint.refreshAll();
        }

    });
});